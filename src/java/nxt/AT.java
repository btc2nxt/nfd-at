/*
 * Some portion .. Copyright (c) 2014 CIYAM Developers

 Distributed under the MIT/X11 software license, please refer to the file license.txt
 in the root project directory or http://www.opensource.org/licenses/mit-license.php.

 */

package nxt;


import nxt.at.AT_API_Helper;
import nxt.at.AT_Constants;
import nxt.at.AT_Controller;
import nxt.at.AT_Exception;
import nxt.at.AT_Machine_State;
import nxt.at.AT_Transaction;
import nxt.util.Convert;
import nxt.util.Listener;
import nxt.util.Logger;
import nxt.Account;
import nxt.TransactionImpl.BuilderImpl;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ConcurrentSkipListMap;

public final class AT extends AT_Machine_State implements Cloneable  {

	static {
		Nxt.getBlockchainProcessor().addListener(new Listener<Block>() {
			@Override
			public void notify(Block block) {
				try {
					if (block.getBlockATs()!=null)
					{
						LinkedHashMap<Long,byte[]> blockATs = AT_Controller.getATsFromBlock(block.getBlockATs());
						for ( Long atId : blockATs.keySet()){
							//Long atId = AT_API_Helper.getLong( id );
							AT at = AT.getAT( atId );
							int prevHeight = at.getPreviousBlock();
							//at.setRunBlock(block.getHeight());
							
							/*
							 *use ringATs record the priority, no need to delete and add
							 *only record the index of the highest index of ringATs 
							 *if it is last, the highest AT is first
							 */
							priorityIndex = ringATs.indexOf(atId);							
							
							at.previousBlock = block.getHeight();
						}
						// if =-1 no AT in blockchain, so ringAT is empty
						if (priorityIndex >-1) {
							priorityIndex = (priorityIndex >= ringATs.size()-1 ? 0 :priorityIndex+1);							
						}
					}
				} catch (AT_Exception e) {
					e.printStackTrace();
				}	

			}

		}, BlockchainProcessor.Event.AFTER_BLOCK_APPLY);
		
		Nxt.getBlockchainProcessor().addListener(new Listener<Block>() {
			@Override
			public void notify(Block block) {
				try {
					if (block.getBlockATs()!=null)
					{
						LinkedHashMap<Long,byte[]> blockATs = AT_Controller.getATsFromBlock(block.getBlockATs());
						if (!blockATs.isEmpty() && blockATs.keySet().iterator().hasNext()) {
							Long atId =  blockATs.keySet().iterator().next();
							priorityIndex = ringATs.indexOf(atId);
							Logger.logDebugMessage("after popped block AT priority: " + priorityIndex);													
						}
					}
				} catch (AT_Exception e) {
					e.printStackTrace();
				}	

			}

		}, BlockchainProcessor.Event.BLOCK_POPPED);		
	}    


	private static ConcurrentMap< Long , AT> ATs = new ConcurrentHashMap<>();
	private static ConcurrentMap<Long, List<AT>> accountATs = new ConcurrentHashMap<>();
	private static Collection<AT> allATs = Collections.unmodifiableCollection(ATs.values());

	//private static ConcurrentSkipListMap< Integer , List< Long > > orderedATs = new ConcurrentSkipListMap<>();
	private static LinkedList<Long> ringATs = new LinkedList<>();	
	private static int priorityIndex=-1; // the index of ringATs has the highest priority

	public static Collection<AT> getAllATs() {
		return allATs;
	}

	protected int getPreviousBlock() {
		return this.previousBlock;
	}

	public static AT getAT(byte[] id) {

		return ATs.get(AT_API_Helper.getLong(id));
	}

	public static AT getAT(Long id) {

		return ATs.get(id);
	}

	public static List<AT> getATsIssuedBy(Long accountId) {
		List<AT> ATs = accountATs.get(accountId);
		if (ATs == null) {
			return Collections.emptyList();
		}
		return Collections.unmodifiableList(ATs);
	}

	static void addAT(Long atId, Long senderAccountId, String name, String description, byte[] machineCode ,byte[] machineData ,byte[] creationBytes , int height) {

		ByteBuffer bf = ByteBuffer.allocate( 8 + 8 );
		bf.order( ByteOrder.LITTLE_ENDIAN );

		bf.putLong( atId );

		byte[] id = new byte[ 8 ];

		bf.putLong( 8 , senderAccountId );

		byte[] creator = new byte[ 8 ];
		bf.clear();
		bf.get( id , 0 , 8 );
		bf.get( creator , 0 , 8);
		AT at = new AT( id , creator , name , description , machineCode, machineData, creationBytes , height );
		if (AT.ATs.putIfAbsent(atId, at) != null) {
			throw new IllegalStateException("AT with id " + Convert.toUnsignedLong(atId) + " already exists");
		}

		List<AT> accountATsList = accountATs.get(senderAccountId);
		if (accountATsList == null) {
			accountATsList = new ArrayList<>();
			accountATs.put(senderAccountId, accountATsList);
		}

/*		if (orderedATs.containsKey(height))
			orderedATs.get(height).add(atId);
		else{
			List<Long> orderedAT = new ArrayList<>();
			orderedAT.add(atId);
			orderedATs.put(height, orderedAT);
		}*/		
		if (!ringATs.contains(atId)){
			if ( ringATs.isEmpty()) {
				ringATs.addLast(atId);
				//first AT index is zero
				priorityIndex = ringATs.indexOf(atId);
			}
			else {
				ringATs.add(priorityIndex++, atId);				
			}
		}
		Logger.logDebugMessage("add new AT, priority: " + priorityIndex);		
		at.setPreviousBlock( height );
		AT_Controller.resetMachine(at);

	}

	private void setPreviousBlock(int height) {
		this.previousBlock = height;

	}

	public static List<Long> getOrderedATs(){
		List<Long> orderedATs = new ArrayList();
		int index;
		if (priorityIndex>-1){
		for (index = priorityIndex; index < ringATs.size(); index++) {
			orderedATs.add(ringATs.get(index));
		}
		}
		for (index = 0; index< priorityIndex; index++) {
			orderedATs.add(ringATs.get(index));
		}
		return orderedATs;
	}

	static void removeAT(Long atId) {
		//AT at = AT.ATs.remove(atId);
		//List<AT> accountATList = accountATs.get(at.getAccountId());
		//accountATList.remove(at);
		int index = ringATs.indexOf(atId);		
		if (index >=0) {
			if ( index <= priorityIndex) {
				priorityIndex--;
			}
			ringATs.remove(atId);
		}
		else {
			Logger.logDebugMessage("remove AT from ringATs error, no AT with id: " + atId);			
		}
			
	}

	static void clear() {
		AT.ATs.clear();
		AT.accountATs.clear();
		AT.ringATs.clear();
		//sleepingATs.clear();
		//activeATs.clear();
		//zeroBalanceATs.clear();
	}

	static boolean isATAccountId(Long Id) {
		return ATs.containsKey(Id);
	}

	private final String name;    
	private final String description;
	private int previousBlock;	


	private AT( byte[] atId , byte[] creator , String name , String description , byte[] machineCode, byte[] machineData, byte[] creationBytes , int height ) {
		super( atId , creator , machineCode, machineData, creationBytes , height );
		this.name = name;
		this.description = description;
		this.previousBlock = 0;

	}

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}

	public byte[] getApCode() {
		return getAp_code().array();
	}

	public byte[] getApData() {
		return getAp_data().array();
	}

}
