/*
 * Copyright (c) 2014 CIYAM Developers

 Distributed under the MIT/X11 software license, please refer to the file license.txt
 in the root project directory or http://www.opensource.org/licenses/mit-license.php.

 */

package nxt.at;

import java.util.LinkedList;
import java.util.List;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentSkipListMap;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;


public class AT_Machine_State
{

	public class Machine_State 
	{
		transient boolean running;
		transient boolean stopped;
		transient boolean finished;

		int pc;
		int pcs;

		transient int opc;

		int cs;
		int us;

		int steps;

		private byte[] A1 = new byte[ 8 ];
		private byte[] A2 = new byte[ 8 ];
		private byte[] A3 = new byte[ 8 ];
		private byte[] A4 = new byte[ 8 ];

		private byte[] B1 = new byte[ 8 ];
		private byte[] B2 = new byte[ 8 ];
		private byte[] B3 = new byte[ 8 ];
		private byte[] B4 = new byte[ 8 ];

		byte[] flags = new byte[ 4 ];

		TreeSet<Integer> jumps = new TreeSet<Integer>();

		Machine_State()
		{
			pcs=0;
			reset();
		}

		boolean isRunning()
		{
			return running;
		}

		void reset()
		{
			pc = pcs;
			opc = 0;
			cs = 0;
			us = 0;
			steps = 0;
			if ( !jumps.isEmpty() )
				jumps.clear();
			stopped = false;
			finished = false;
		}

		void run()
		{
			running = true;
		}
		
		public long getSteps()
		{
			return steps;
		}

		protected byte[] getMachineStateBytes()
		{

			int size = 4 + 1 + 4 + 4 + 4 + 4 + 4;
			ByteBuffer bytes = ByteBuffer.allocate(size);
			bytes.order( ByteOrder.LITTLE_ENDIAN );
			bytes.put( flags );
			bytes.put( ( byte ) ( machineState.running == true ? 1 : 0 ) );
			bytes.putInt( machineState.pc );
			bytes.putInt( machineState.pcs );
			bytes.putInt( machineState.cs );
			bytes.putInt( machineState.us );
			bytes.putInt( machineState.steps );
			/*Iterator< Integer > iter = machineState.jumps.iterator();
			while ( iter.hasNext() )
			{
				bytes.putInt( iter.next() );
			}
			 */
			return bytes.array();
		}
	}

	private short version;

	private long g_balance;
	private long p_balance;  // can use for temporary balance e.g. in lottery case

	private Machine_State machineState;

	private int csize;
	private int dsize;
	private int c_user_stack_bytes;
	private int c_call_stack_bytes;

	private byte[] atID = new byte[ AT_Constants.AT_ID_SIZE ];
	private byte[] creator = new byte[ AT_Constants.AT_ID_SIZE ];

	private long minimumFee;
	private int creationBlockHeight;
	private int waitForNumberOfBlocks;
	private boolean freezeWhenSameBalance;
	private int sleepBetween;	
	private volatile int runBlock;  //when block reaches this block, AT will run.  
	private volatile long stepsFeeNQT;  //an AT should pay steps fee when run one time
	private volatile int retrievedHeight;  //last call 0x0304 function	
	// active at the blockHeight	
	private int activeBlockHeight;
	private volatile int lastRanBlock;  //AT ran at last block.
	private static int lastRanSteps;  //ATs has ran steps before the next AT.	
	
	/*
	 * <height,blockATbytes> for pop AT blocks, because AT doesn't know previous blcokATs
	 * maximized blockATbytes is 720 blocks
	 */
	private ConcurrentSkipListMap< Integer , byte[] > blockATBytesAtHeights = new ConcurrentSkipListMap<>();		

	private transient ByteBuffer ap_data;
	private transient ByteBuffer ap_code;

	private LinkedList<AT_Transaction> transactions;

	public AT_Machine_State( byte[] atId , int cSize , byte[] machineCode , int dSize , byte[] machineData , int stackSize , long minimumFee , int creationBlockHeight , int waitForNumberOfBlocks , int sleepBetweenBlocks )
	{
		/*this.atID = atId;
		this.csize = cSize;
		this.dsize = dSize;
		this.c_call_stack_bytes = stackSize;
		this.c_user_stack_bytes = stackSize;

		this.ap_code = ByteBuffer.allocate(csize);
		this.ap_code.order(ByteOrder.LITTLE_ENDIAN);
		this.ap_code.put(machineCode);
		this.ap_data = ByteBuffer.allocate(dsize+c_call_stack_bytes+c_user_stack_bytes);
		this.ap_data.order(ByteOrder.LITTLE_ENDIAN);
		this.ap_data.put(machineData);
		this.ap_code.clear();
		this.ap_data.clear();

		this.g_balance = 0L;
		this.p_balance = 0L;

		this.machineState = new Machine_State();
		this.minimumFee = minimumFee;

		this.creationBlockHeight = creationBlockHeight;
		this.waitForNumberOfBlocks = waitForNumberOfBlocks;
		this.sleepBetweenBlocks = sleepBetweenBlocks;
		transactions = new LinkedList<>();*/
	}

	public AT_Machine_State( byte[] atId , byte[] creator ,  byte[] machineCode, byte[] machineData, byte[] creationBytes , int height ) 
	{
		this.version = AT_Constants.getInstance().AT_VERSION( height );
		this.atID = atId;
		this.creator = creator;
		//256
		int pageSize = ( int ) AT_Constants.getInstance().PAGE_SIZE( height );
		
		ByteBuffer b = ByteBuffer.allocate( creationBytes.length );
		b.order( ByteOrder.LITTLE_ENDIAN );
		
		b.put( creationBytes );
		b.clear();
		
		this.version = b.getShort();
		b.getShort(); //future: reserved for future needs
		
		short callStackPages = b.getShort();
		short userStackPages = b.getShort();
		this.waitForNumberOfBlocks = b.getInt();
		this.sleepBetween = b.getInt();		
		this.freezeWhenSameBalance = b.get()== 0 ? false : true;
		this.activeBlockHeight = b.getInt();	
		this.retrievedHeight = Integer.MAX_VALUE;
		this.stepsFeeNQT = 0;

		int codePages = (int)Math.ceil((float)machineCode.length / pageSize) +1;
		int dataPages = (int)Math.ceil((float)machineData.length / pageSize) ;
		
		this.csize = codePages * pageSize;
		this.dsize = dataPages * pageSize;
		this.c_call_stack_bytes = callStackPages * pageSize;
		this.c_user_stack_bytes = userStackPages * pageSize;

		this.ap_code = ByteBuffer.allocate( csize );
		this.ap_code.order( ByteOrder.LITTLE_ENDIAN );
		this.ap_code.put( machineCode );
	
		this.ap_data = ByteBuffer.allocate( this.dsize + this.c_call_stack_bytes + this.c_user_stack_bytes );
		this.ap_data.order( ByteOrder.LITTLE_ENDIAN );
		this.ap_data.put( machineData );

		this.creationBlockHeight = height;
		this.minimumFee = ( codePages + 
				dataPages + 
				callStackPages +
				userStackPages ) * AT_Constants.getInstance().COST_PER_PAGE( height );

		this.transactions = new LinkedList<>();
		this.g_balance = 0;
		this.p_balance = 0;
		this.machineState = new Machine_State();
	}

	protected byte[] get_A1()
	{
		return machineState.A1;
	}

	protected byte[] get_A2()
	{
		return machineState.A2;
	}

	protected byte[] get_A3()
	{
		return machineState.A3;
	}

	protected byte[] get_A4()
	{
		return machineState.A4;
	}

	protected byte[] get_B1()
	{
		return machineState.B1;
	}

	protected byte[] get_B2()
	{
		return machineState.B2;
	}

	protected byte[] get_B3()
	{
		return machineState.B3;
	}

	protected byte[] get_B4()
	{
		return machineState.B4;
	}

	protected void set_A1( byte[] A1 )
	{
		this.machineState.A1 = A1.clone();
	}

	protected void set_A2( byte[] A2 ){
		this.machineState.A2 = A2.clone();
	}

	protected void set_A3( byte[] A3 )
	{
		this.machineState.A3 = A3.clone();
	}

	protected void set_A4( byte[] A4 )
	{
		this.machineState.A4 = A4.clone();
	}

	protected void set_B1( byte[] B1 )
	{
		this.machineState.B1 = B1.clone();
	}

	protected void set_B2( byte[] B2 )
	{
		this.machineState.B2 = B2.clone();
	}

	protected void set_B3( byte[] B3 )
	{
		this.machineState.B3 = B3.clone();
	}

	protected void set_B4( byte[] B4 )
	{
		this.machineState.B4 = B4.clone();
	}

	protected void addTransaction(AT_Transaction tx)
	{
		transactions.add(tx);
	}

	protected void clearTransactions()
	{
		transactions.clear();
	}

	public LinkedList<AT_Transaction> getTransactions()
	{
		return transactions;
	}

	protected ByteBuffer getAp_code() 
	{
		return ap_code;
	}

	public ByteBuffer getAp_data() 
	{
		return ap_data;
	}

	protected int getC_call_stack_bytes() 
	{
		return c_call_stack_bytes;
	}

	protected int getC_user_stack_bytes() 
	{
		return c_user_stack_bytes;
	}

	protected int getCsize() 
	{
		return csize;
	}

	protected int getDsize() 
	{
		return dsize;
	}

	public Long getG_balance() 
	{
		return g_balance;
	}

	public Long getP_balance() 
	{
		return p_balance;
	}

	public byte[] getId()
	{
		return atID;
	}

	public Long getLongId()
	{
		byte[] bId = getId();
		byte[] b = new byte[ 8 ];
		for ( int i = 0; i < 8; i++ )
		{
			b[ i ] = bId[ i ];
		}

		Long atLongId = AT_API_Helper.getLong( b );
		return atLongId;
	}
	
	public Machine_State getMachineState() 
	{
		return machineState;
	}

	public long getMinimumFee()
	{
		return minimumFee;
	}

	protected void setC_call_stack_bytes(int c_call_stack_bytes) 
	{
		this.c_call_stack_bytes = c_call_stack_bytes;
	}

	protected void setC_user_stack_bytes(int c_user_stack_bytes) 
	{
		this.c_user_stack_bytes = c_user_stack_bytes;
	}

	protected void setCsize(int csize) 
	{
		this.csize = csize;
	}

	protected void setDsize(int dsize) 
	{
		this.dsize = dsize;
	}

	public void setG_balance(Long g_balance) 
	{
		this.g_balance = g_balance;
	}

	public void setP_balance(Long p_balance) 
	{
		this.p_balance = p_balance;
	}

	public void setMachineState(Machine_State machineState) 
	{
		this.machineState = machineState;
	}

	public void setWaitForNumberOfBlocks(int waitForNumberOfBlocks) 
	{
		this.waitForNumberOfBlocks = waitForNumberOfBlocks;
	}

	public int getWaitForNumberOfBlocks()
	{
		return this.waitForNumberOfBlocks;
	}

	public byte[] getCreator() 
	{
		return this.creator;
	}

	public int getCreationBlockHeight() 
	{
		return this.creationBlockHeight;
	}

	public boolean freezeOnSameBalance()
	{
		return this.freezeWhenSameBalance;
	}
	
	public short getVersion()
	{
		return version;
	}

	public byte[] getTransactionBytes( )
	{
		ByteBuffer b = ByteBuffer.allocate( (creator.length + 8 ) * transactions.size() );
		b.order( ByteOrder.LITTLE_ENDIAN );
		for (AT_Transaction tx : transactions )
		{
			b.put( tx.getRecipientId() );
			b.putLong( tx.getAmountNQT() );
		}
		return b.array();

	}

	public byte[] getBytes()
	{
		byte[] txBytes = getTransactionBytes();
		byte[] stateBytes = machineState.getMachineStateBytes();
		byte[] dataBytes = ap_data.array();

		ByteBuffer b = ByteBuffer.allocate( atID.length + txBytes.length + stateBytes.length + dataBytes.length );
		b.order( ByteOrder.LITTLE_ENDIAN );

		b.put( atID );
		b.put( stateBytes );
		b.put( dataBytes );
		b.put( txBytes );

		return b.array();

	}
	
	public byte[] getBytesWithoutTransactions()
	{
		byte[] stateBytes = machineState.getMachineStateBytes();
		byte[] dataBytes = ap_data.array();

		ByteBuffer b = ByteBuffer.allocate( stateBytes.length + dataBytes.length +8 +4);
		b.order( ByteOrder.LITTLE_ENDIAN );

		b.put( stateBytes );
		b.put( dataBytes );
		b.putLong(stepsFeeNQT);
		b.putInt(retrievedHeight);	

		return b.array();
	}
	
	public void setStateAndData(byte[] stateAndData) {
		ByteBuffer b = ByteBuffer.allocate( stateAndData.length );
		b.order( ByteOrder.LITTLE_ENDIAN );
		
		b.put(  stateAndData );
		b.clear();

		b.get( machineState.flags );
		machineState.running = (b.get()==1 ? true :false );	
		machineState.pc = b.getInt();
		machineState.pcs = b.getInt();
		machineState.cs = b.getInt();
		machineState.us = b.getInt();
		machineState.steps = b.getInt();
		
		byte[] machineData = new byte[ this.dsize + this.c_call_stack_bytes + this.c_user_stack_bytes ];
		b.get(machineData);
		this.ap_data.order( ByteOrder.LITTLE_ENDIAN );
		this.ap_data.put( machineData );
		
		stepsFeeNQT =b.getLong();
		b.getInt(retrievedHeight);
	}
	
	public long getStepsFeeNQT(){
		return stepsFeeNQT;
	}
	
	public void setRunBlock(int runBlock){
		this.runBlock= runBlock;// + this.sleepBetween;
	}        

	public void setRunBlock(){
		this.runBlock= this.runBlock + this.sleepBetween;
	}        

	public void setStepsFeeNQT(long stepsFeeNQT){
		this.stepsFeeNQT= stepsFeeNQT;
	}
	
	public int getRetrievedHeight(){
		return retrievedHeight;
	}
	
	public void setRetrievedHeight( int retrievedHeight){
		this.retrievedHeight = retrievedHeight;
	}

	public ConcurrentSkipListMap< Integer , byte[] > getBlockATBytesAtHeights() {
		return this.blockATBytesAtHeights;	
	}

	public int getLastRanBlock(){
		return this.lastRanBlock;
	}
	
	public void setLastRanBlock(int blockHeight){
		this.lastRanBlock= blockHeight;
	}        

	public int getLastRanSteps(){
		return this.lastRanSteps;
	}
	
	public void setLastRanSteps(int lastRanSteps){
		this.lastRanSteps= lastRanSteps;
	}        
	
}
