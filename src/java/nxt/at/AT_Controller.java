package nxt.at;


import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.NavigableSet;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ConcurrentSkipListMap;

import nxt.AT;
import nxt.Account;
import nxt.Constants;
import nxt.Nxt;
import nxt.util.Convert;
import nxt.util.Logger;

public abstract class AT_Controller {

	private static volatile boolean timeStampRetrieved;	
	
	public static int runSteps( AT_Machine_State state )
	{
		state.getMachineState().finished = false;
		state.getMachineState().steps = 0;
		state.getAp_code().clear();		
		//an AT has run
		timeStampRetrieved = false;
		AT_API_Platform_Impl.setTimeStampListToNull();

		AT_Machine_Processor processor = new AT_Machine_Processor( state );

		int height = Nxt.getBlockchain().getHeight();
		int lastRanSteps = state.getLastRanSteps();

		while ( state.getMachineState().steps < (AT_Constants.getInstance().MAX_STEPS( height ) - lastRanSteps))
		{
			long stepFee = AT_Constants.getInstance().STEP_FEE( height );

			if ( ( state.getG_balance() < stepFee ) )
			{
				Logger.logDebugMessage( "stopped - not enough balance" );
				return 3;
			}

			state.setG_balance( state.getG_balance() - stepFee );
			state.getMachineState().steps++;
			int rc = processor.processOp( false , false );

			if ( rc >= 0 )
			{
				if ( state.getMachineState().stopped )
				{
					//TODO add freeze
					Logger.logDebugMessage( "stopped" );
					return 2;
				}
				else if ( state.getMachineState().finished )
				{
					Logger.logDebugMessage( "finished" );
					return 1;
				}
			}
			else
			{
				if ( rc == -1 )
					Logger.logDebugMessage( "error: overflow" );
				else if ( rc==-2 )
					Logger.logDebugMessage( "error: invalid code" );
				else
					Logger.logDebugMessage( "unexpected error" );
				return 0;
			}
		}
		return 5;
	}

	public static void resetMachine( AT_Machine_State state ) {
		state.getMachineState( ).reset( );
		listCode( state , true , true );
	}

	public static void listCode( AT_Machine_State state , boolean disassembly , boolean determine_jumps ) {

		AT_Machine_Processor machineProcessor = new AT_Machine_Processor( state );

		int opc = state.getMachineState().pc;
		int osteps = state.getMachineState().steps;

		state.getAp_code().order( ByteOrder.LITTLE_ENDIAN );
		state.getAp_data().order( ByteOrder.LITTLE_ENDIAN );

		state.getMachineState( ).pc = 0;
		state.getMachineState( ).opc = opc;

		while ( true )
		{
			int rc= machineProcessor.processOp( disassembly , determine_jumps );
			if ( rc<=0 ) break;

			state.getMachineState().pc += rc;
		}

		state.getMachineState().steps = osteps;
		state.getMachineState().pc = opc;
	}

	public static int checkCreationBytes( byte[] machineCode, byte[] machineData, byte[] creation , int height ) throws AT_Exception{
		int totalPages = 0;
		try 
		{
			AT_Constants instance = AT_Constants.getInstance();
			int pageSize = ( int ) instance.PAGE_SIZE( height );
			
			int codePages = (int)Math.ceil((float)machineCode.length / pageSize) ;
			if ( codePages > instance.MAX_MACHINE_CODE_PAGES( height ) )
			{
				throw new AT_Exception( AT_Error.INCORRECT_CODE_PAGES.getDescription() );
			}

			int dataPages = (int)Math.ceil((float)machineData.length / pageSize) ;
			if ( dataPages > instance.MAX_MACHINE_DATA_PAGES( height ) )
			{
				throw new AT_Exception( AT_Error.INCORRECT_DATA_PAGES.getDescription() );
			}
			
			ByteBuffer b = ByteBuffer.allocate( creation.length );
			b.order( ByteOrder.LITTLE_ENDIAN );
			
			b.put(  creation );
			b.clear();

			short version = b.getShort();
			if ( version > instance.AT_VERSION( height ) )
			{
				throw new AT_Exception( AT_Error.INCORRECT_VERSION.getDescription() );
			}

			short reserved = b.getShort(); //future: reserved for future needs

			short callStackPages = b.getShort();
			if ( callStackPages > instance.MAX_MACHINE_CALL_STACK_PAGES( height ) )
			{
				throw new AT_Exception( AT_Error.INCORRECT_CALL_PAGES.getDescription() );
			}

			short userStackPages = b.getShort();
			if ( userStackPages > instance.MAX_MACHINE_USER_STACK_PAGES( height ) )
			{
				throw new AT_Exception( AT_Error.INCORRECT_USER_PAGES.getDescription() );
			}
			
			totalPages = codePages + dataPages + userStackPages + callStackPages;
			Logger.logDebugMessage("codePages: " + codePages + "totalpage: "+ totalPages);			
			/*if ( ( codePages + dataPages + userStackPages + callStackPages ) * instance.COST_PER_PAGE( height ) < txFeeAmount )
			{
				return AT_Error.INCORRECT_CREATION_FEE.getCode();
			}*/
			
			//TODO note: run code in demo mode for checking if is valid

		} catch ( BufferUnderflowException e ) 
		{
			throw new AT_Exception( AT_Error.INCORRECT_CREATION_TX.getDescription() );
		}
		return totalPages;
	}
	
	/*
	 * decide AT will run or not
	 * after runsteps, will generate fee txs
	 */
			
	public static int getATResult(AT at,Long atId, int currentBlockHeight, int orderedATHeight, Long recipientId ) {

		ConcurrentSkipListMap< Integer , byte[] > blockATBytesAtHeights = at.getBlockATBytesAtHeights();		
		/*
		 * handle pop block
		 */
		if (at.getLastRanBlock() >= currentBlockHeight) {
			Logger.logDebugMessage("Block is popped to height "+currentBlockHeight+ " from height: "+ at.getLastRanBlock() );
			if (blockATBytesAtHeights.containsKey(currentBlockHeight - 1 )){
				at.setStateAndData(blockATBytesAtHeights.get(currentBlockHeight - 1) );
			}
		}
		
		long atAccountBalance = getATAccountBalance( atId );
		long atStateBalance = at.getG_balance() + at.getStepsFeeNQT();

		AT_API_Platform_Impl.setEndHeight(currentBlockHeight-1);
		
		//AT's balance doesnt change, skip
		if ( at.freezeOnSameBalance() && atAccountBalance == atStateBalance )
		{
			return 0;
		}

		//update g_balance, because of new tx sending money to AT's account
		if (atAccountBalance != atStateBalance) {
			at.setG_balance( atAccountBalance - at.getStepsFeeNQT());
		}

		if ( ( at.getWaitForNumberOfBlocks() == 0 || at.getWaitForNumberOfBlocks() > 0 && currentBlockHeight - at.getCreationBlockHeight() >= at.getWaitForNumberOfBlocks()) &&
				atAccountBalance >= AT_Constants.getInstance().STEP_FEE( currentBlockHeight ) )
		{
			Logger.logDebugMessage("AT is running"+ atId.toString());					
			at.clearTransactions();
			//waiting for blocks after create an AT 
			if (at.getWaitForNumberOfBlocks() > 0) {
				at.setWaitForNumberOfBlocks( 0 );				
			}
			Long currentBalance = at.getG_balance();
			
			runSteps ( at );
			
			Long totalQuantityNQT= 0L;
			for (AT_Transaction atTx : at.getTransactions()){
				totalQuantityNQT += atTx.getAmountNQT();
				totalQuantityNQT += atTx.getFeeNQT();							
			}
			
			/*
			 * totalfee of this AT, not pay to forger
			 * steps fee= gBalanceBefore C gBalanceAFter - sum(amount)-sum(fee)
			 * process totalfee
			 * if totalfee >= 2 nxt then create a new tx to send to forger
			 * else left to next time to pay to forger
			 */					
			long totalFee =at.getStepsFeeNQT()+ currentBalance - at.getG_balance() - totalQuantityNQT;
			if (!at.getTransactions().isEmpty()) {
				AT_Transaction atTx = at.getTransactions().get(0);
				atTx.setFeeNQT(totalFee + atTx.getFeeNQT());
				totalFee = 0;
			} 
			
			else if (totalFee >= Constants.MAX_AUTOMATED_TRANSATIONS_STEP_FEE_NQT) {
				//amount must > 0
				AT_Transaction tx = new AT_Transaction(atId,recipientId,1L,totalFee - 1);
				at.addTransaction(tx);
				totalFee = 0;
			}
			//save unpaid stepFee to next forger, because the fee is too small
			at.setStepsFeeNQT(totalFee);
			//steps fee of all AT, looks like this variant useless
			//totalFeeNQT += totalFee;
			at.setP_balance( at.getG_balance() );
			
			//save at state to blockatbytesATHeights
			at.setLastRanBlock(currentBlockHeight);				
			if (!blockATBytesAtHeights.containsKey(currentBlockHeight)){
				blockATBytesAtHeights.put(currentBlockHeight,at.getBytesWithoutTransactions());

				if (blockATBytesAtHeights.containsKey( currentBlockHeight - AT_Constants.AT_POP_BLOCK))
				{
					blockATBytesAtHeights.remove(currentBlockHeight - AT_Constants.AT_POP_BLOCK);
				}
				
			}						
			
			return getCostOfOneAT();			
		}
		else
			return 0;
	}
	public static AT_Block getCurrentBlockATs( int freePayload , int blockHeight, List< AT > processedATs, Long forgerAccountId){

		//ConcurrentSkipListMap< Integer , List< Long > > orderedATs = AT.getOrderedATs();
		//NavigableSet< Integer > heightSet = orderedATs.keySet();
		//Iterator< Integer > keys = heightSet.iterator();

		//List< AT > processedATs = new ArrayList< >();

		int costOfOneAT = getCostOfOneAT();
		int atCost;
		int payload = 0;
		int totalSteps = 0; 
		
		Logger.logDebugMessage("AT is generating BlockATs");
		int orderedATHeight = 0;//keys.next();
		List< Long > ats = AT.getOrderedATs();

		Iterator< Long > ids = ats.iterator();
		while ( payload < freePayload - costOfOneAT && ids.hasNext() )
		{
				Long atId = ids.next();
				AT at = AT.getAT( atId );
				at.setLastRanSteps(totalSteps);
				//if have txs, must update payload 				
				atCost =getATResult(at,atId,blockHeight, orderedATHeight,forgerAccountId );
				if (atCost >0 ) {
						payload += atCost;											
						processedATs.add( at );
						totalSteps += at.getMachineState().steps;
						
				} //end of if(atCost>0)
				
				if (totalSteps >= AT_Constants.getInstance().MAX_STEPS( blockHeight )) {
					break;
				}
		}

		long totalAmount = 0;
		for ( AT at : processedATs )
		{
			totalAmount = makeTransactions( at );
		}

		byte[] bytesForBlock = null;

		try
		{
			bytesForBlock = getBlockATBytes( processedATs , payload );
		}
		catch ( NoSuchAlgorithmException e )
		{
			//should not reach ever here
			e.printStackTrace();
		}

		AT_Block atBlock = new AT_Block( totalSteps * AT_Constants.getInstance().STEP_FEE( blockHeight ) , totalAmount , bytesForBlock );

		return atBlock;
	}

	public static AT_Block validateATs( byte[] blockATs , int blockHeight, Long generatorId ) throws NoSuchAlgorithmException, AT_Exception {

		int lastRunBlock = 0;
		
		LinkedHashMap< Long , byte[] > ats = getATsFromBlock( blockATs );
		
		List< AT > processedATs = new ArrayList< >();
		
		boolean validated = true;
		long totalSteps = 0;
		int atCost;		
		MessageDigest digest = MessageDigest.getInstance( "MD5" );
		byte[] md5 = new byte[ getCostOfOneAT() ];
		
		for ( Long atId : ats.keySet() )
		{
			AT at = AT.getAT( atId );
			//Long atId =AT_API_Helper.getLong( atIdBytes );
			atCost = getATResult(at,atId,blockHeight, lastRunBlock,generatorId );
			
			if (atCost >0 ) {
				processedATs.add( at );
				totalSteps += at.getMachineState().steps;				
											
				md5 = digest.digest( at.getBytes() );
				if ( !Arrays.equals( md5 , ats.get( atId ) ) )
				{
					Logger.logDebugMessage("Calculated md5 and recieved md5 are not matching");				
					throw new AT_Exception( "Calculated md5 and recieved md5 are not matching" );
				}
			}
		}
		
		long totalAmount = 0;
		for ( AT at : processedATs )
		{
			//calcualate total amount
			totalAmount = makeTransactions( at );
		}

		AT_Block atBlock = new AT_Block( totalSteps * AT_Constants.getInstance().STEP_FEE( blockHeight ) , totalAmount , new byte[ 1 ] , validated );

		return atBlock;
	}

	public static LinkedHashMap< Long , byte[] > getATsFromBlock( byte[] blockATs ) throws AT_Exception
	{
		if ( blockATs.length > 0 )
		{
			if ( blockATs.length % (getCostOfOneAT() ) != 0 )
			{
				throw new AT_Exception("blockATs must be a multiple of cost of one AT ( " + getCostOfOneAT() +" )" );
			}
		}
		
		ByteBuffer b = ByteBuffer.wrap( blockATs );
		b.order( ByteOrder.LITTLE_ENDIAN );

		byte[] temp = new byte[ AT_Constants.AT_ID_SIZE ];
		byte[] md5 = new byte[ 16 ];

		//LinkedHashMap< byte[] , byte[] > ats = new LinkedHashMap<>();
		LinkedHashMap< Long , byte[] > ats = new LinkedHashMap<>();

		while ( b.position() < b.capacity() )
		{
			b.get( temp , 0 , temp.length );
			b.get( md5 , 0 , md5.length );			
			ats.put( AT_API_Helper.getLong(temp) , md5.clone() );			
		}
		
		if ( b.position() != b.capacity() )
		{
			throw new AT_Exception("bytebuffer not matching");
		}
		
		return ats;
	}

	private static byte[] getBlockATBytes(List<AT> processedATs , int payload ) throws NoSuchAlgorithmException {

		ByteBuffer b = ByteBuffer.allocate( payload );
		b.order( ByteOrder.LITTLE_ENDIAN );
		byte[] md5 = new byte[ getCostOfOneAT() ];
		
		MessageDigest digest = MessageDigest.getInstance( "MD5" );
		for ( AT at : processedATs )
		{
			b.put( at.getId() );
			md5 = digest.digest( at.getBytes() );			
			//digest.update( at.getBytes() );
			//b.put( digest.digest() );
			b.put(md5);
		}

		return b.array();
	}

	private static int getCostOfOneAT() {
		return AT_Constants.AT_ID_SIZE + 16;
	}
	
	//platform based implementations
	//platform based 
	private static long makeTransactions( AT at ) {
		long totalAmount = 0;
		for (AT_Transaction tx : at.getTransactions() )
		{
			totalAmount += tx.getAmountNQT();
			Logger.logInfoMessage("Transaction to " + Convert.toUnsignedLong(AT_API_Helper.getLong(tx.getRecipientId())) + " amount " + tx.getAmountNQT() );
			
		}
		return totalAmount;
	}
	
	/*private static void saveTransaction(Connection con, TransactionImpl transaction)
	{
		 try {
	                try (PreparedStatement pstmt = con.prepareStatement("INSERT INTO transaction (id, deadline, sender_public_key, "
	                        + "recipient_id, amount, fee, referenced_transaction_full_hash, height, "
	                        + "block_id, signature, timestamp, type, subtype, sender_id, attachment_bytes, "
	                        + "block_timestamp, full_hash, version, has_message, has_encrypted_message, has_public_key_announcement, "
	                        + "has_encrypttoself_message, ec_block_height, ec_block_id) "
	                        + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)")) {
	                    int i = 0;
	                    pstmt.setLong(++i, transaction.getId());
	                    pstmt.setShort(++i, transaction.getDeadline());
	                    pstmt.setBytes(++i, transaction.getSenderPublicKey());
	                    if (transaction.getType().hasRecipient() && transaction.getRecipientId() != null) {
	                        pstmt.setLong(++i, transaction.getRecipientId());
	                    } else {
	                        pstmt.setNull(++i, Types.BIGINT);
	                    }
	                    pstmt.setLong(++i, transaction.getAmountNQT());
	                    pstmt.setLong(++i, transaction.getFeeNQT());
	                    if (transaction.getReferencedTransactionFullHash() != null) {
	                        pstmt.setBytes(++i, Convert.parseHexString(transaction.getReferencedTransactionFullHash()));
	                    } else {
	                        pstmt.setNull(++i, Types.BINARY);
	                    }
	                    pstmt.setInt(++i, transaction.getHeight());
	                    pstmt.setLong(++i, transaction.getBlockId());
	                    pstmt.setBytes(++i, transaction.getSignature());
	                    pstmt.setInt(++i, transaction.getTimestamp());
	                    pstmt.setByte(++i, transaction.getType().getType());
	                    pstmt.setByte(++i, transaction.getType().getSubtype());
	                    pstmt.setLong(++i, transaction.getSenderId());
	                    int bytesLength = 0;
	                    for (Appendix appendage : transaction.getAppendages()) {
	                        bytesLength += appendage.getSize();
	                    }
	                    if (bytesLength == 0) {
	                        pstmt.setNull(++i, Types.VARBINARY);
	                    } else {
	                        ByteBuffer buffer = ByteBuffer.allocate(bytesLength);
	                        buffer.order(ByteOrder.LITTLE_ENDIAN);
	                        for (Appendix appendage : transaction.getAppendages()) {
	                            appendage.putBytes(buffer);
	                        }
	                        pstmt.setBytes(++i, buffer.array());
	                    }
	                    pstmt.setInt(++i, transaction.getBlockTimestamp());
	                    pstmt.setBytes(++i, Convert.parseHexString(transaction.getFullHash()));
	                    pstmt.setByte(++i, transaction.getVersion());
	                    pstmt.setBoolean(++i, transaction.getMessage() != null);
	                    pstmt.setBoolean(++i, transaction.getEncryptedMessage() != null);
	                    pstmt.setBoolean( ++i, false );
	                    pstmt.setBoolean(++i, transaction.getEncryptToSelfMessage() != null);
	                    pstmt.setInt(++i, transaction.getECBlockHeight());
	                    if (transaction.getECBlockId() != null) {
	                        pstmt.setLong(++i, transaction.getECBlockId());
	                    } else {
	                        pstmt.setNull(++i, Types.BIGINT);
	                    }
	                    pstmt.executeUpdate();
	                }
	        } catch (SQLException e) {
	            throw new RuntimeException(e.toString(), e);
	        }
	}*/
	
	

	//platform based
	

	//platform based
	private static long getATAccountBalance( Long id ) {
		//Long accountId = AT_API_Helper.getLong( id );
		Account atAccount = Account.getAccount( id );

		if ( atAccount != null )
		{
			return atAccount.getBalanceNQT();
		}

		return 0;

	}

	public static boolean getTimeStampRetrieved(){
		return timeStampRetrieved;
	}

	public static void setTimeStampRetrieved(boolean retrievedTimeStamp1){
		timeStampRetrieved = retrievedTimeStamp1;
	}

}
