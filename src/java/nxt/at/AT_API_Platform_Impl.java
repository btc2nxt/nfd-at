package nxt.at;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.security.MessageDigest;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import nxt.AT;
import nxt.Account;
import nxt.Db;
import nxt.Nxt;
import nxt.Transaction;
import nxt.crypto.Crypto;
import nxt.util.Convert;
import nxt.util.Logger;
import nxt.Constants;

//NXT,NFD API IMPLEMENTATION

public class AT_API_Platform_Impl extends AT_API_Impl {

	private final static AT_API_Platform_Impl instance = new AT_API_Platform_Impl();

	private static List<Long> timeStampList = new ArrayList<>();
	private static int timeStampIndex;
	private static int startHeight;
	private static int endHeight;	
	private static int numOfTx;		

	AT_API_Platform_Impl()
	{

	}

	public static AT_API_Platform_Impl getInstance()
	{
		return instance;
	}

	@Override
	public long get_Block_Timestamp( AT_Machine_State state ) 
	{

		int height = Nxt.getBlockchain().getHeight();
		return AT_API_Helper.getLongTimestamp( height , 0 );

	}

	public long get_Creation_Timestamp( AT_Machine_State state ) //0x0301
	{
		//we don't process last block, have to wait confirmations
		Long atId = state.getLongId();
		AT at =AT.getAT(atId); 
		//startHeight = Nxt.getBlockchain().getLastBlock().getHeight()-1 ;//- at.getWaitForNumberOfBlocks() - 1;//at.getSleepBetween();
		//endHeight =startHeight + 1;// + at.getSleepBetween();
		//lastHeight =0;
		
		return AT_API_Helper.getLongTimestamp( state.getCreationBlockHeight() , 0 );
	}

	@Override
	public long get_Last_Block_Timestamp( AT_Machine_State state ) 
	{

		int height = Nxt.getBlockchain().getHeight() - 1;
		return AT_API_Helper.getLongTimestamp( height , 0 );
	}

	@Override
	public void put_Last_Block_Hash_In_A( AT_Machine_State state ) {
		ByteBuffer b = ByteBuffer.allocate( state.get_A1().length * 4 );
		b.order( ByteOrder.LITTLE_ENDIAN );

		b.put( Nxt.getBlockchain().getLastBlock().getPreviousBlockHash() );

		byte[] temp = new byte[ 8 ];

		b.get( temp, 0 , 8 );
		state.set_A1( temp );

		b.get( temp , 0 , 8 );
		state.set_A2( temp );

		b.get( temp , 0 , 8 );
		state.set_A3( temp );

		b.get( temp , 0 , 8 );
		state.set_A4( temp );


	}

	@Override
	/*  0x0304
	 *  if can not find a tx , store atId -> A3, now height+num -> A4
	 *  when read timestamp from A,	if A1==0 and A3= atId, then timestamp=A4
	 */
	public void A_to_Tx_after_Timestamp( long val , AT_Machine_State state ) {

		int height = AT_API_Helper.longToHeight( val );
		int numOfTx = AT_API_Helper.longToNumOfTx( val );

		Long atId = state.getLongId();		
		Long transactionId = 0L;
		Logger.logDebugMessage("get tx after timestamp "+val + " height: "+ height+" atId: "+ atId);
		
		//when AT first runs the function, or re-catch the timestamp, e.g. refunding 
		if (!AT_Controller.getTimeStampRetrieved() || height < state.getRetrievedHeight()){ 
			startHeight = AT_API_Helper.longToHeight( val );
			numOfTx = AT_API_Helper.longToNumOfTx( val );
			timeStampList = findATTransactions(startHeight, atId, numOfTx);
			AT_Controller.setTimeStampRetrieved(true);
			timeStampIndex = 0;
			
			AT at =AT.getAT(atId);
			//endHeight =blockHeight + at.getSleepBetween() +at.getWaitForNumberOfBlocks();				
			//endHeight =startHeight + 1;// +at.getWaitForNumberOfBlocks();
			//endHeight = Nxt.getBlockchain().getLastBlock().getHeight();//- at.getWaitForNumberOfBlocks() - 1;//at.getSleepBetween();			
		}
			
		//more then one block
		while (startHeight <= endHeight) {
			Logger.logDebugMessage("loop.... "+ startHeight + "-" + numOfTx);				
			if (timeStampList.isEmpty()) {
				numOfTx = 0;
				startHeight++;
				if (startHeight > endHeight){
					break;
				}
				timeStampList = findATTransactions(startHeight, atId, numOfTx);
				timeStampIndex = 0;					
			}
			if (!timeStampList.isEmpty() && timeStampList.size()>timeStampIndex){
				transactionId = timeStampList.get(timeStampIndex++);
				numOfTx++;
				break;
			}
			//get txs from next block				
			if (transactionId ==0){
				timeStampList.clear();
			}	
		}
		// if this block contain zero tx, next time will scan next block
		//if (startHeight > endHeight ) {
		//	startHeight= endHeight ;
		//}
						
		state.setRetrievedHeight( startHeight);
		
		Logger.logInfoMessage("tx with id "+transactionId+" found");
		clear_A( state );
		state.set_A1( AT_API_Helper.getByteArray( transactionId ) );
		// can not find a tx
		if (transactionId == 0) {
			state.set_A3( AT_API_Helper.getByteArray( atId ) );
			state.set_A4( AT_API_Helper.getByteArray( AT_API_Helper.getLongTimestamp( startHeight, 0 ) ) );			
		}
	}

	@Override
	public long get_Type_for_Tx_in_A( AT_Machine_State state ) {
		long txid = AT_API_Helper.getLong( state.get_A1() );

		Transaction tx = Nxt.getBlockchain().getTransaction( txid );

		if ( tx != null )
		{
			if (tx.getType().getType() == 1 )
			{
				return 1;
			}
		}
		return 0;
	}

	@Override  //0x036
	public long get_Amount_for_Tx_in_A( AT_Machine_State state ) {
		long txId = AT_API_Helper.getLong( state.get_A1() );

		Transaction tx = Nxt.getBlockchain().getTransaction( txId );
		long amount = 0;
		if ( tx != null )
		{
			amount = tx.getAmountNQT();
		}
		return amount;
	}

	@Override //0x0307
	public long get_Timestamp_for_Tx_in_A( AT_Machine_State state ) {
		long txId = AT_API_Helper.getLong( state.get_A1() );
		Logger.logInfoMessage("get timestamp for tx with id " + txId + " found");
		Transaction tx;
		
		if (txId != 0) {
			tx = Nxt.getBlockchain().getTransaction( txId );			
		} else 
			tx = null;

		if ( tx != null )
		{
			int blockHeight = tx.getHeight();

			byte[] bId = state.getId();
			byte[] b = new byte[ 8 ];
			for ( int i = 0; i < 8; i++ )
			{
				b[ i ] = bId[ i ];
			}

			int txHeight = findTransactionHeight( txId , blockHeight , AT_API_Helper.getLong( b ) );

			return AT_API_Helper.getLongTimestamp( blockHeight , txHeight );
		}
		else {
			//Long atId = ;	
			Long atId = AT_API_Helper.getLong(state.getId());
			if ( atId == AT_API_Helper.getLong( state.get_A3() ) ) {
				Logger.logDebugMessage("get timestamp from A4");				
				return AT_API_Helper.getLong( state.get_A4() );				
			}
			else
			  return AT_API_Helper.getLongTimestamp( startHeight+1 , 0 );
			//return AT_API_Helper.getLongTimestamp( Integer.MAX_VALUE , Integer.MAX_VALUE );
		}
	}

	@Override
	public long get_Ticket_Id_for_Tx_in_A( AT_Machine_State state ) {
		long txId = AT_API_Helper.getLong( state.get_A1() );

		Transaction tx = Nxt.getBlockchain().getTransaction( txId );

		if ( tx !=null )
		{
			int txBlockHeight = tx.getHeight();


			int blockHeight = Nxt.getBlockchain().getHeight();

			if ( blockHeight - txBlockHeight < AT_Constants.getInstance().BLOCKS_FOR_TICKET( blockHeight ) ){ //for tests - for real case 1440
				state.setWaitForNumberOfBlocks( (int)AT_Constants.getInstance().BLOCKS_FOR_TICKET( blockHeight ) - ( blockHeight - txBlockHeight ) );
				state.getMachineState().pc -= 11;
				state.setG_balance( 0L ); // hack to halt and continue from that point
				return 0;
			}

			MessageDigest digest = Crypto.sha256();

			byte[] senderPublicKey = tx.getSenderPublicKey();

			ByteBuffer bf = ByteBuffer.allocate( 2 * Long.SIZE + senderPublicKey.length );
			bf.order( ByteOrder.LITTLE_ENDIAN );
			bf.putLong( Nxt.getBlockchain().getLastBlock().getId() );
			bf.putLong( tx.getId() );
			bf.put( senderPublicKey);

			digest.update(bf.array());
			byte[] byteTicket = digest.digest();

			long ticket = Math.abs( AT_API_Helper.getLong( byteTicket ) );

			Logger.logDebugMessage( "info: ticket for txid: " + Convert.toUnsignedLong( tx.getId() ) + "is: " + ticket );
			return ticket;
		}
		return 0;
	}

	@Override
	public long message_from_Tx_in_A_to_B( AT_Machine_State state ) {
		long txid = AT_API_Helper.getLong( state.get_A1() );

		Transaction tx = Nxt.getBlockchain().getTransaction( txid );
		byte[] message = tx.getMessage().getMessage();

		if ( message.length > state.get_A1().length * 4 )
			return 0;

		ByteBuffer b = ByteBuffer.allocate( state.get_A1().length * 4 );
		b.order( ByteOrder.LITTLE_ENDIAN );
		b.put( message );
		b.clear();

		byte[] temp = new byte[ 8 ];

		b.get( temp, 0 , 8 );
		state.set_B1( temp );

		b.get( temp , 0 , 8 );
		state.set_B2( temp );

		b.get( temp , 0 , 8 );
		state.set_B3( temp );

		b.get( temp , 0 , 8 );
		state.set_B4( temp );

		return 1;
	}
	
	@Override  //0x030a
	public long B_to_Address_of_Tx_in_A( AT_Machine_State state ) {

		long tx = AT_API_Helper.getLong( state.get_A1() );
		
		long address = Nxt.getBlockchain().getTransaction( tx ).getSenderId();

		clear_B( state );

		state.set_B1( AT_API_Helper.getByteArray( address ) );

		return 1;
	}

	@Override
	public void B_to_Address_of_Creator( AT_Machine_State state ) {
		long creator = AT_API_Helper.getLong( state.getCreator() );

		clear_B( state );

		state.set_B1( AT_API_Helper.getByteArray( creator ) );

	}

	@Override //0x0400
	public long get_Current_Balance( AT_Machine_State state ) {
		long balance = Account.getAccount( AT_API_Helper.getLong(state.getId()) ).getBalanceNQT();
		return balance;
	}

	@Override
	public long get_Previous_Balance( AT_Machine_State state ) {
		return state.getP_balance();
	}

	@Override  //0x402
	public long send_to_Address_in_B( long val , AT_Machine_State state ) {
		
		long atId = AT_API_Helper.getLong( state.getId() );
		AT at = AT.getAT( atId );
		if ( at.getG_balance() >= Constants.ONE_NXT ) { 
			if ( at.getG_balance() >= val ) {
				AT_Transaction tx = new AT_Transaction((long)0, state.get_B1().clone() , val - Constants.ONE_NXT, Constants.ONE_NXT );
				state.addTransaction( tx );
				at.setG_balance( at.getG_balance() - val );
			}
			else {
				AT_Transaction tx = new AT_Transaction((long)0, state.get_B1().clone() , at.getG_balance()-Constants.ONE_NXT, Constants.ONE_NXT );			
				state.addTransaction( tx );
				at.setG_balance( 0L );
				//at.setG_balance( at.getG_balance() - 123 );
			}
		}
		return 1;
	}

	@Override  //0403
	public long send_All_to_Address_in_B( AT_Machine_State state ) {
		
		long atId = AT_API_Helper.getLong( state.getId() );
		AT at = AT.getAT( atId );
		if ( at.getG_balance() >= Constants.ONE_NXT ) {
			AT_Transaction tx = new AT_Transaction( (long)0, state.get_B1().clone() , at.getG_balance()-Constants.ONE_NXT, Constants.ONE_NXT );
			state.addTransaction( tx );
			at.setG_balance( 0L );			
		}

		return 1;
	}

	@Override
	public long send_Old_to_Address_in_B( AT_Machine_State state ) {
		
		AT at = AT.getAT( state.getId() );
		
		if ( at.getP_balance() > at.getG_balance()  )
		{
			AT_Transaction tx = new AT_Transaction( (long)0, state.get_B1() , state.getG_balance(), (long)0 );
			state.addTransaction( tx );
			
			at.setG_balance( 0L );
			at.setP_balance( 0L );
		
		}
		else
		{
			AT_Transaction tx = new AT_Transaction( (long)0,state.get_B1() , state.getP_balance(),(long)0 );
			state.addTransaction( tx );
			
			at.setG_balance( at.getG_balance() - at.getP_balance() );
			at.setP_balance( 0l );
			
		}

		return 1;
	}

	@Override
	public long send_A_to_Address_in_B( AT_Machine_State state ) {
		
		AT at = AT.getAT( state.getId() );

		long amount = AT_API_Helper.getLong( state.get_A1() );

		if ( at.getG_balance() > amount )
		{
		
			AT_Transaction tx = new AT_Transaction( (long)0,state.get_B1() , amount,(long)0 );
			state.addTransaction( tx );
			
			state.setG_balance( state.getG_balance() - amount );
			
		}
		else
		{
			AT_Transaction tx = new AT_Transaction((long)0, state.get_B1() , at.getG_balance(),(long)0 );
			state.addTransaction( tx );
			
			state.setG_balance( 0L );
		}
		return 1;
	}

	public long add_Minutes_to_Timestamp( long val1 , long val2 , AT_Machine_State state) {
		int height = AT_API_Helper.longToHeight( val1 );
		int numOfTx = AT_API_Helper.longToNumOfTx( val1 );
		int addHeight = height + (int) (val2 / AT_Constants.getInstance().AVERAGE_BLOCK_MINUTES( Nxt.getBlockchain().getHeight() ));

		return AT_API_Helper.getLongTimestamp( addHeight , numOfTx );
	}

    //get txs in a block with index > numOfTx
    public static List<Long> findATTransactions(int startHeight ,Long atID, int numOfTx){
    	try (Connection con = Db.getConnection();
    			PreparedStatement pstmt = con.prepareStatement("SELECT id FROM transaction WHERE height= ? and recipient_id = ?")){
    		pstmt.setInt(1, startHeight);
    		pstmt.setLong(2, atID);
    		ResultSet rs = pstmt.executeQuery();
    		Long transactionId = 0L;
    		List<Long> transactionIdList = new ArrayList<>();
    		
    		int counter = 1;
    		while (rs.next()) {
                if (counter > numOfTx){
                    transactionId = rs.getLong("id");
                    transactionIdList.add(transactionId);
                }
            }
            rs.close();
            return transactionIdList;
    		
    	} catch (SQLException e) {
    		throw new RuntimeException(e.toString(), e);
    	}
    	
    }

	protected static int findTransactionHeight(Long transactionId, int height, Long atID){
		try (Connection con = Db.getConnection();
				PreparedStatement pstmt = con.prepareStatement("SELECT id FROM transaction WHERE height= ? and recipient_id = ?")){
			pstmt.setInt( 1, height );
			pstmt.setLong( 2, atID );
			ResultSet rs = pstmt.executeQuery();

			int counter = 0;
			while ( rs.next() ) {
				if (rs.getLong( "id" ) == transactionId){
					counter++;
					break;
				}
				counter++;
			}
			rs.close();
			return counter;

		} catch ( SQLException e ) {
			throw new RuntimeException(e.toString(), e);
		}
	}

	public static void setTimeStampListToNull(){
		if (!timeStampList.isEmpty()) {
			timeStampList.clear();;
		}
	}

	public static void setEndHeight(int lastBlockHeight) {
		endHeight = lastBlockHeight;
	}
}
