package nxt.at;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.SortedMap;
import java.util.TreeMap;

public class AT_Transaction {
	/**
	 * 
	 */
	private static SortedMap<Long,SortedMap<Long,AT_Transaction>> all_AT_Txs = new TreeMap<>();
	private Long senderId;
	private byte[] recipientId = new byte[ AT_Constants.AT_ID_SIZE ];
	private Long amountNQT;
	private Long feeNQT;	
	
	public AT_Transaction(Long senderId, byte[] recipientId, Long amountNQT, Long feeNQT){
		this.senderId = senderId;
		this.recipientId = recipientId.clone();
		this.amountNQT = amountNQT;
		this.feeNQT = feeNQT;		
	}
	
	public AT_Transaction(Long senderId, Long recipientId, Long amountNQT, Long feeNQT){
		this.senderId = senderId;
		this.amountNQT = amountNQT;
		this.feeNQT = feeNQT;
		
		ByteBuffer bf = ByteBuffer.allocate( 8 );
		bf.order( ByteOrder.LITTLE_ENDIAN );
		bf.putLong( 0 , recipientId );
		bf.get( this.recipientId , 0 , 8);
	}
		
	public AT_Transaction(byte[] senderId, Long recipientId, Long amountNQT, Long feeNQT){
		this.amountNQT = amountNQT;
		this.feeNQT = feeNQT;
		
		ByteBuffer bf = ByteBuffer.allocate( 8 );
		bf.order( ByteOrder.LITTLE_ENDIAN );
		bf.putLong( 0 , recipientId );
		bf.get( this.recipientId , 0 , 8);

		
		ByteBuffer b = ByteBuffer.allocate( senderId.length );
		b.order( ByteOrder.LITTLE_ENDIAN );
		b.put(  senderId );
		b.clear();
		this.senderId = b.getLong();
	}

	public Long getAmountNQT(){
		return amountNQT;
	}
	
	public Long getSenderId(){
		return senderId;
	}
	
	public byte[] getRecipientId(){
		return recipientId;
	}
	
	public Long getFeeNQT(){
		return feeNQT;
	}

	public void setAmountNQT(Long amountNQT){
		this.amountNQT = amountNQT;
	}
	
	public void setFeeNQT(Long feeNQT){
		this.feeNQT = feeNQT;
	}
	
	public void addTransaction( long atId , Long height) {
		
		
		if (all_AT_Txs.containsKey(atId)){
			all_AT_Txs.get(atId).put(height, this);
		}
		else
		{
			SortedMap< Long , AT_Transaction > temp = new TreeMap<>();
			temp.put( (Long) height , this );
			all_AT_Txs.put( atId , temp );
		}
		
	}
	
	public static AT_Transaction getATTransaction(Long atId, Long height){
		if (all_AT_Txs.containsKey(atId)){
			return all_AT_Txs.get(atId).get(height);
		}
		return null;
	}
	
}
